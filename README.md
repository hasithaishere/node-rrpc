# Node-RRPC

Promise focused RPC using Redis and Node.

## Features

* Easily connect to existing project. No need to rewrite anything specific, just attach to services, that's it! (see examples).
* Supports promises.
* Very simple usage.
* More features planned to implemented, see [TODO](TODO.md).

## Quick Start

### Install
```
npm i node-rrpc
```

## Basic usage

````javascript
const { RRPCServer, RRPCClient } = require('node-rrpc');

const rpcServer = new RRPCServer({ channel: 'my_rpc_channel' });
const rpcClient = new RRPCClient({ channel: 'my_rpc_channel' });

/******** callee side **********/

function sayHello (name) {
  return `hello ${name}!`;
}

function sum (x, y) {
  return x + y;
}

// register handlers like this
rpcServer.handlers = {
  sayHello,
};

// or this
rpcServer.attachHandler('sum', sum);

// connect to redis instance
rpcServer.connect().then(() => {
  // now server is up and running (connected reversely, not listening to anything).
  console.log('rpc server ready');
});

/******** caller side **********/

rpcClient.connect().then(() => {
  console.log('connected to rpc channel');

  const sayHello = rpcClient.createInvoker('sayHello');
  const sum = rpcClient.createInvoker('sum');

  sayHello('RPC').then(console.log); // => 'Hello RPC!'
  sum(1,2).then(console.log); // => 3
});
````

See `examples/showcase.js` for advanced usage.

## Integrating with existing API.

Consider we have two files, let's say, `api.js` and `index.js`.

### api.js
````javascript
function myFunction () {
  // ...
}

module.exports = {
  myFunction,
};
````

### index.js

````javascript
const { RRPCServer } = require('node-rrpc');
const api = require('./api.js');

const rpcServer = new RRPCServer({ channel: 'my_rpc_channel' });

// attaching functions is pretty easy.
rpcServer.handlers = api;

// connect to channel.
rpcServer.connect().then(() => {
  console.log('connection successful! up and running.');
});
````

`RRPCClient` is now able to execute functions from `api.js` on channel `my_rpc_channel`.

For example, consider we have `RRPCClient` running on a different server, but of course, they're both connected to same redis instance.

## 
````javascript
const { RRPCClient } = require('node-rrpc');

const rpcClient = new RRPCClient({ channel: 'my_rpc_channel' });

rpcClient.connect().then(() => {
  // connection successful, we can now execute our API like a breeze.

  // first, we create `invoker`.
  const myFunction = rpcClient.createInvoker('myFunction');

  // and we use it like a regular function.
  myFunction().then((response) => { console.log(response); });
});
````

API overview
============

## Contents
- [RRPCClient](#rrpcclient)
  - [constructor](#rrpcclient-constructor)
  - [connect](#rrpcclient-connect)
  - [createInvoker](#rrpcclient-createinvoker)
- RRPCServer
  - [constructor](#rrpcserver-constructor)
  - [connect](#rrpcserver-connect)
  - [handlers](#rrpcserver-handlers)
  - [attachHandler](#rrpcserver-attachhandler)
  - [detachHandler](#rrpcserver-detachhandler)


## RRPCClient

#### new RRPCClient(options)

Creates a RPCClient instance.

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>Object</code> |  | General options |
| options.channel | <code>String</code> | | RPC communication channel. |
| [options.host] | <code>String</code> | <code>&quot;localhost&quot;</code> | Host of the Redis server. |
| [options.port] | <code>number</code> | <code>6379</code> | Port of the Redis server. |
| [options.ioredisOpts] | <code>Object</code> | | Additional ioredis parameters. |
| [options.secure] | <code>Boolean</code> | <code>false</code> | Secure communication (sends signatures along with messages). |
| [options.key] | <code>String</code> | | RSA private key (required if secure option is set to true). |

#### connect([cb]): Promise

Subscribes to RPC channel, returns promise. If cb is passed, calls cb(err, count) where count is how many channels it subscribed.

#### createInvoker(fn, [opts]): Promise

Creates a wrapper for RPC function.

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| fn | <code>String</code> |  | Function name which was registered by RRPCServer. |
| [opts] | <code>Object</code> | | General options. |
| [opts.timeoutMS] | <code>Number</code> | <code>1000</code> | Timeout in MS. If timeout exceeds this property's value, timeout error is thrown. |
| [opts.isCallback] | <code>Boolean</code> | <code>false</code> | If function that was registered by RRPCServer is callback style (i.e does not return value, but calls callback), set this to true. Note that first argument of that function should be `err`. |

Returns `function wrapper` which we can use like a regular function.

NOTES: By default it returns promises, but if you pass `function` type parameter in the end, it will call callback instead. DO NOT PASS FUNCTION IN THE END if you want to use promises or it will not work.

i.e:

````javascript
// pseudo code
const fn = createInvoker('myfn', { isCallback: true, timeoutMS: 1000 });
fn().then(...).catch(...); // OK.
fn(() => {}).then(...).catch(...); // Not OK.

// instead do this:
fn((err, ...args) => { /* handle it here */ }); // OK.
````

This is currently limitation. If you pass last parameter `function` type, then it assumes that it should call callback rather than return a promise.

## RRPCServer

#### new RRPCServer(options)

Creates a RRPCServer instance.

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>Object</code> |  | General options |
| options.channel | <code>String</code> | | RPC communication channel. |
| [options.host] | <code>String</code> | <code>&quot;localhost&quot;</code> | Host of the Redis server. |
| [options.port] | <code>number</code> | <code>6379</code> | Port of the Redis server. |
| [options.ioredisOpts] | <code>Object</code> | | Additional ioredis parameters. |
| [options.secure] | <code>Boolean</code> | <code>false</code> | Secure communication (sends signatures along with messages). |
| [options.key] | <code>String</code> | | RSA public key (required if secure option is set to true). |

#### connect([cb]): Promise

Subscribes to RPC channel, returns promise. If cb is passed, calls cb(err, count) where count is how many channels it subscribed.

#### handlers

Getter and setter to apply multiple handlers.

i.e:
````javascript
// pseudo code
server.handlers = {
  registeredName: function() { ... },
};
````

#### attachHandler(name, fn): void

Attaches handler to be called.

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>String</code> |  | Registered name for a function. |
| fn | <code>Function</code> | | Function to be called `name` |

#### detachHandler(name): void

Detaches handler.

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>String</code> |  | Registered name for a function. |

***

## Limitations

* Currently there is no way to pass functions as arguments (but planned!) ~~and any object that is not serializable~~, but it's work in progress for now.
* No multithread support (planned to improve). Because if N instances join on the same channel as a listeners, they all get the request and return response.
* Currently serialization works for Date and `pseudo serializes` error objects.

## Built With

* [Redis](https://redis.io/) - Redis is an in-memory database that persists on disk. The data model is key-value, but many different kind of values are supported: Strings, Lists, Sets, Sorted Sets, Hashes, HyperLogLogs, Bitmaps.
* [ioredis](https://github.com/luin/ioredis) - A robust, performance-focused and full-featured Redis client for Node.js.

## License
MIT
