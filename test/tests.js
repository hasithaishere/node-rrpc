const assert = require('assert');

const { RRPCServer, RRPCClient } = require('../lib');

const rpcServer = new RRPCServer({ channel: '_test_channel_' });
const rpcClient = new RRPCClient({ channel: '_test_channel_' });

function add(x, y) {
  return x + y;
}

function echo_handler(str) {
  return str;
}

function dummy_handler() { }

describe('Server', () => {
  describe('#connect', () => {
    it('should connect to channel without crash', async () => {
      await rpcServer.connect();
    });
  });

  describe('handlers', () => {
    it('should create regular function handler with setter', async () => {
      rpcServer.handlers = {
        add,
        dummy_handler,
      };

      assert.equal(rpcServer.handlers['add'], add);
      assert.equal(rpcServer.handlers['dummy_handler'], dummy_handler);
    });

    it('should create regular handler with #attachHandler', () => {
      rpcServer.attachHandler('echo_handler', echo_handler);

      assert.equal(rpcServer.handlers['echo_handler'], echo_handler);
    });

    it('should remove dummy handler with #detachHandler', () => {
      rpcServer.detachHandler('dummy_handler');

      assert.equal(rpcServer.handlers['dummy_handler'], undefined);
    });
  });
});
