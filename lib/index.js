const RRPCClient = require('./client');
const RRPCServer = require('./server');

module.exports = {
  RRPCClient,
  RRPCServer,
};
