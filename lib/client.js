const crypto = require('crypto');
const ioredis = require('ioredis');
const _ = require('lodash');
const msgpack = require('msgpack-lite');
const debug = require('debug');

const serializer = require('./serializer');
const signature = require('./signature');

class RRPCClient {
  /**
  * @param {Object} opts - options
  * @param {String} opts.host - redis host
  * @param {Number} opts.port - redis port
  * @param {String} opts.channel - channel on where to execute RPC.
  * @param {Object} opts.ioredisOpts - additional ioredis params.
  * @param {Boolean} opts.secure - secure or not (good if redis is available to public).
  * @param {String} opts.key - RSA private key (required if secure option is set to true).
  */
  constructor(opts) {
    opts = Object.assign({}, opts);
    this.opts = opts;

    this.opts.ioredisOpts = {
      retryStrategy: function (times) {
        return Math.min(times * 1000, 5000);
      }
    };

    this.opts.host = this.opts.host || '127.0.0.1';
    this.opts.port = this.opts.port || 6379;
    this.opts.channel = this.opts.channel || crypto.randomBytes(16).toString('hex');
    this.opts.secure = !!this.opts.secure;
    this.opts.ioredisOpts = Object.assign(this.opts.ioredisOpts, opts.ioredisOpts);

    this.__onMessage = this.__onMessage.bind(this);

    this.__pub = new ioredis(this.opts.port, this.opts.host, this.opts.ioredisOpts);
    this.__sub = new ioredis(this.opts.port, this.opts.host, this.opts.ioredisOpts);

    this.__sub.on('messageBuffer', this.__onMessage);
    this.__sub.on('error', this.__onSubError);
    this.__pub.on('error', this.__onPubError);

    this.__reqs = {};
  }

  __onPubError(err) {
    const d = debug('RRPCServer:__onPubError');

    if (err.code === 'ECONNREFUSED') {
      return d('could not connect to server');
    }
  }

  __onSubError(err) {
    const d = debug('RRPCServer:__onSubError');

    if (err.code === 'ECONNREFUSED') {
      return d('could not connect to server');
    }
  }

  __onMessage(channel, message) {
    const d = debug('RRPCClient:__onMessage');
    const ch = `${this.opts.channel}:response`;

    d(`got message on channel: "${channel}", message: "${message}"`);

    // avoid broadcasted messages.
    if (channel.toString() !== ch) {
      return d(`channel didn't match.`);
    }

    try {
      message = msgpack.decode(message);
    } catch (err) {
      return d(`invalid packet received: ${message}`);
    }

    if (!message.__rpc__) {
      return d(`not a RPC packet, ignoring.`);
    } else if (!message.uuid) {
      return d(`uuid not present`);
    }

    const req = this.__reqs[message.uuid];
    if (!req) {
      return d('request was not found.');
    }

    d('clearing rejection timer');
    clearTimeout(req.rejectionTimer);

    message.args = !Array.isArray(message.args) ? [] : message.args;
    message.args = serializer.deserialize(message.args);

    try {
      if (req.cb) {
        d('calling callback handler');

        req.cb.apply(null, message.args);
      } else {
        d('handling with promises');
        const err = message.args[0];

        d('calling promise handler');

        if (err) {
          d('error response, rejecting');
          req.reject(err);
        } else {
          d('success response, resolving.');
          req.resolve(message.args.length < 3 ? message.args.slice(1)[0] : message.args.slice(1));
        }
      }
    } catch (err) {
      d('error calling handler', err);
    } finally {
      // cleanup.
      this.__reqs = _.omit(this.__reqs, message.uuid);
    }
  }

  connect(cb) {
    const d = debug('RRPCClient:connect');

    d(`subscribing to channel ${this.opts.channel}:response`);

    // TODO: release old subscriber if present.
    return new Promise((resolve, reject) => {
      this.__sub.subscribe(`${this.opts.channel}:response`, (err, cnt) => {
        if (err) {
          d('error subscribing, reason:', err);
          if (cb) return cb(err);
          return reject(err);
        } else {
          d(`success. subscribed channel count: ${cnt}`);
          if (cb) return cb(null, cnt);
          return resolve(cnt);
        }
      });
    });
  }

  /**
  * @param {Object} opts - options
  * @param {Number} opts.timeoutMS - timeout in milliseconds (defaults to 1000ms).
  * @param {Boolean} opts.isCallback - set true if calling callback function (defaults to false).
  */
  createInvoker(fn, opts) {
    const d = debug('RRPCClient:createInvoker');
    opts = _.assign({}, opts);

    opts.timeoutMS = parseInt(opts.timeoutMS);
    opts.timeoutMS = isNaN(opts.timeoutMS) ? 1000 : opts.timeoutMS;

    opts.isCallback = !!opts.isCallback;

    if (typeof fn !== 'string') {
      d(`invalid fn argument: ${fn}`);
      throw new Error('invalid fn argument');
    }

    return (...args) => {
      const d = debug('RRPCClient:invoke');

      const { timeoutMS, isCallback } = opts;
      const [cb] = args.slice(-1);
      let invokerHasCallback = false;

      const publishMessage = (uuid, args, reject) => {
        args = args && args.map(arg => serializer.serialize(arg));
        let sig;

        if (this.opts.secure && this.opts.key) {
          try {
            sig = signature.sign(uuid, this.opts.key);
          } catch (err) {
            return reject(err);
          }
        }

        this.__pub.publish(`${this.opts.channel}:request`, msgpack.encode({
          __rpc__: true,
          isCallback,
          uuid,
          fn,
          args,
          sig,
        })).catch(reject);
      };

      if (typeof cb === 'function') {
        invokerHasCallback = true;
        args.pop();
      }

      const uuid = crypto.randomBytes(16).toString('hex');
      this.__reqs[uuid] = {};

      if (invokerHasCallback) {
        // callback style handling.
        d('handling with callback');

        this.__reqs[uuid].cb = cb;
        this.__reqs[uuid].rejectionTimer = setTimeout(() => cb(new Error('RPC timed out')), timeoutMS);

        publishMessage(uuid, args, cb);
        // TODO: resolve or reject?
        return Promise.resolve();
      } else {
        // promise style handling.
        d('handling with promises');

        return new Promise((resolve, reject) => {
          this.__reqs[uuid].resolve = resolve;
          this.__reqs[uuid].reject = reject;
          this.__reqs[uuid].rejectionTimer = setTimeout(() => reject(new Error('RPC timed out.')), timeoutMS);

          publishMessage(uuid, args, reject);
        });
      }
    };
  }
};

module.exports = RRPCClient;