const crypto = require('crypto');
const ioredis = require('ioredis');
const _ = require('lodash');
const msgpack = require('msgpack-lite');
const debug = require('debug');
const serializeError = require('serialize-error');

const serializer = require('./serializer');
const signature = require('./signature');

class RRPCServer {
  /**
  * @param {Object} opts - options
  * @param {String} opts.host - redis host
  * @param {Number} opts.port - redis port
  * @param {String} opts.channel - channel on where to execute RPC.
  * @param {Object} opts.ioredisOpts - additional ioredis params.
  * @param {Boolean} opts.secure - secure or not (good if redis is available to public).
  * @param {String} opts.key - RSA public key (required if secure option is set to true).
  */
  constructor(opts) {
    opts = Object.assign({}, opts);
    this.opts = opts;

    this.opts.ioredisOpts = {
      retryStrategy: function (times) {
        return Math.min(times * 1000, 5000);
      }
    };

    this.opts.host = this.opts.host || '127.0.0.1';
    this.opts.port = this.opts.port || 6379;
    this.opts.channel = this.opts.channel || crypto.randomBytes(16).toString('hex');
    this.opts.secure = !!this.opts.secure;
    this.opts.ioredisOpts = Object.assign(this.opts.ioredisOpts, opts.ioredisOpts);

    this.__onMessage = this.__onMessage.bind(this);

    this.__pub = new ioredis(this.opts.port, this.opts.host, this.opts.ioredisOpts);
    this.__sub = new ioredis(this.opts.port, this.opts.host, this.opts.ioredisOpts);

    this.__sub.on('messageBuffer', this.__onMessage);
    this.__sub.on('error', this.__onSubError);
    this.__pub.on('error', this.__onPubError);

    this.__handlers = {};
  }

  __onPubError(err) {
    const d = debug('RRPCServer:__onPubError');

    if (err.code === 'ECONNREFUSED') {
      return d('could not connect to server');
    }
  }

  __onSubError(err) {
    const d = debug('RRPCServer:__onSubError');

    if (err.code === 'ECONNREFUSED') {
      return d('could not connect to server');
    }
  }

  __onMessage(channel, message) {
    const d = debug('RRPCServer:__onMessage');
    const ch = `${this.opts.channel}:request`;

    d(`got message on channel: "${channel}", message: "${message}"`);

    // avoid broadcasted messages.
    if (channel.toString() !== ch) {
      return d(`channel didn't match.`);
    }

    try {
      message = msgpack.decode(message);
      // message.resp = message.resp && msgpack.decode(message.resp);
    } catch (err) {
      return d(`invalid packet received: ${message}`);
    }

    if (!message.__rpc__) {
      return d(`not a RPC packet, ignoring.`);
    } else if (!message.fn || !this.__handlers[message.fn]) {
      return d(`handler not present/found.`);
    }

    d('calling handler');

    const responseHandler = (...args) => {
      d('handler responded, serializing data before sending');

      args = serializer.serialize(args);

      d('serialized data', args);

      this.__pub.publish(`${this.opts.channel}:response`, msgpack.encode({
        __rpc__: true,
        uuid: message.uuid,
        args,
      }));
    };

    let args = message.args;
    args = args && args.map(arg => serializer.deserialize(arg));

    if (args && !Array.isArray(args)) {
      args = [args];
    }

    // check if we need to verify message before moving to handler part.
    if (this.opts.secure && message.sig) {
      try {
        if (!this.opts.key) {
          return responseHandler(new Error('public key is missing'));
        } else if (!signature.verify(message.uuid, this.opts.key, message.sig)) {
          return responseHandler(new Error('signature check failed'));
        }
      } catch (err) {
        return responseHandler(err);
      }
    }

    try {
      const handler = this.__handlers[message.fn];

      if (message.isCallback) {
        d('calling callback-style function');
        return handler.apply(null, [...args, responseHandler]);
      }

      const res = handler.apply(null, args);

      if (typeof res.then === 'function') {
        res
          .then(responseHandler.bind(null, false))
          .catch(responseHandler.bind(null));
      } else {
        responseHandler(false, res);
      }
    } catch (err) {
      responseHandler(err);
    }
  }

  connect(cb) {
    const d = debug('RRPCServer:connect');

    d(`subscribing to channel ${this.opts.channel}:request`);

    // TODO: release old subscriber if present.
    return new Promise((resolve, reject) => {
      this.__sub.subscribe(`${this.opts.channel}:request`, (err, cnt) => {
        if (err) {
          d('error subscribing, reason:', err);
          if (cb) return cb(err);
          return reject(err);
        } else {
          d(`success. subscribed channel count: ${cnt}`);
          if (cb) return cb(null, cnt);
          return resolve(cnt);
        }
      });
    });
  }

  attachHandler(event, fn) {
    this.__handlers[event] = fn;
  }

  detachHandler(event) {
    this.__handlers = _.omit(this.__handlers, event);
  }

  get handlers() {
    return this.__handlers;
  }

  set handlers(v) {
    this.__handlers = v;
  }
}

module.exports = RRPCServer;