#### Roadmap

* ~~Error and Date object serialization.~~
* Queued RPC (when disconnected, after reconnecting it will execute queued RPC).
* ~~Some sort of security layer (to avoid arbitrary command injection when open to public).~~
* Bi-directional execution to pass callbacks to functions(?).
* Make it multi-thread safe.
* More and better tests.
* ~~Support for callback style API.~~
* Support for event style API (streaming).
* Ping support.